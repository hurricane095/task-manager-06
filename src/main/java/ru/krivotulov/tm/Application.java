package ru.krivotulov.tm;

import ru.krivotulov.tm.constant.ArgumentConst;
import ru.krivotulov.tm.constant.TerminalConst;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Application {

    public static void main(String[] args) {
        if (runArgument(args)) System.exit(0);
        displayWelcome();
        process();
    }

    private static void process() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            try {
                while (true) {
                    System.out.println("ENTER COMMAND:");
                    runCommand(bufferedReader.readLine());
                }
            } finally {
                bufferedReader.close();
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
    }

    private static void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case (TerminalConst.CMD_HELP):
                displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                displayAbout();
                break;
            case (TerminalConst.CMD_EXIT):
                close();
                break;
            default:
                displayError(command);
                break;
        }
    }

    private static boolean runArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String param = args[0];
        runArgument(param);
        return true;
    }

    private static void runArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case (ArgumentConst.CMD_HELP):
                displayHelp();
                break;
            case (ArgumentConst.CMD_VERSION):
                displayVersion();
                break;
            case (ArgumentConst.CMD_ABOUT):
                displayAbout();
                break;
            default:
                displayError(arg);
                break;
        }
    }

    private static void displayHelp() {
        System.out.printf("%s, %s - Display program version. \n",
                TerminalConst.CMD_VERSION,
                ArgumentConst.CMD_VERSION);
        System.out.printf("%s, %s - Display developer info. \n",
                TerminalConst.CMD_ABOUT,
                ArgumentConst.CMD_ABOUT);
        System.out.printf("%s, %s - Display list of terminal commands. \n",
                TerminalConst.CMD_HELP,
                ArgumentConst.CMD_HELP);
        System.out.printf("%s - Close application. \n", TerminalConst.CMD_EXIT);
    }

    private static void displayVersion() {
        System.out.println("1.6.0");
    }

    private static void displayAbout() {
        System.out.println("Aleksey Krivotulov");
        System.out.println("akrivotulov@tsconsulting.com");
    }

    private static void displayError(String arg) {
        System.err.printf("Error! This argument `%s` not supported... \n", arg);
    }

    private static void close() {
        System.exit(0);
    }

}
